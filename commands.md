# Comamnds

1. Create new .Net Solution
```bash
dotnet new sln -n "Solution name"
```

2. Create new C# Console Application
```bash
dotnet new console -n "Project name"
```

3. Create new class library (optional)
```bash
dotnet new classlib -n "Class library name"
```

4. Add project to solution
```bash
dotnet sln "<solution name>" add "<project file>"
```

5. Let VS-Code add required assets

6. Run project
```bash
cd <project name>
```
```bash
dotnet run
```