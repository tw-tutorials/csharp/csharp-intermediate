namespace Polymorphism
{
    public class Rectangle : Shape
    {
        public override void Draw()
        {
            System.Console.WriteLine("Draw a rectangle.");
        }
    }
}
