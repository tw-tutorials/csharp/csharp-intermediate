﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add(1);
            list.Add("Mosh");

            list<int> anotherList = new list<int>();
            anotherList.Add(123);
        }
    }
}
