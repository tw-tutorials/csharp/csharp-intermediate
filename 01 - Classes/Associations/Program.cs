﻿using System;

namespace Associations
{
    class Program
    {
        static void Main(string[] args)
        {
            DbMigrator dbMigrator = new DbMigrator(new Logger());

            Logger logger = new Logger();
            Installer installer = new Installer(logger);

            dbMigrator.Migrate();
            installer.Install();
        }
    }
}
